-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: demo_soap
-- ------------------------------------------------------
-- Server version	8.0.29-0ubuntu0.21.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20220215103532','2022-02-15 10:35:55',87),('DoctrineMigrations\\Version20220302080002','2022-03-02 08:00:19',73),('DoctrineMigrations\\Version20220302082124','2022-03-02 08:21:55',112),('DoctrineMigrations\\Version20220302082453','2022-03-02 08:25:00',44);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `price_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7D053A93D614C7E7` (`price_id`),
  CONSTRAINT `FK_7D053A93D614C7E7` FOREIGN KEY (`price_id`) REFERENCES `price` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,2,'Aventure',''),(2,1,'Découverte','');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantite` int DEFAULT NULL,
  `menu_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F5299398CCD7E912` (`menu_id`),
  CONSTRAINT `FK_F5299398CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,1,2),(2,1,1);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plat`
--

DROP TABLE IF EXISTS `plat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plat` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int DEFAULT NULL,
  `menu_id` int DEFAULT NULL,
  `publish` tinyint(1) NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2038A207C54C8C93` (`type_id`),
  KEY `IDX_2038A207CCD7E912` (`menu_id`),
  CONSTRAINT `FK_2038A207C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  CONSTRAINT `FK_2038A207CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plat`
--

LOCK TABLES `plat` WRITE;
/*!40000 ALTER TABLE `plat` DISABLE KEYS */;
INSERT INTO `plat` VALUES (1,'Ceviche de Daurade',1,2,1,'avec ses kiwis et ses suprême d\'agrumes\r\nau vinaigre de framboise'),(2,'Burger de poisson au pain cacaoté',2,2,1,'Mayonnaise au vinaigre de figues et \r\ncompote d\'oignons au chocolat'),(3,'Brownie au chocolat revisité',3,2,1,'Chantilly à l\'avocat'),(4,'Qimen Haoya',4,2,1,'Thé Noir de Chine Fruité et Boisé aux notes Florales\r\net délicatement Cacaotées'),(5,'Sencha Ariake',4,2,1,'Thé vert Japonais à la fois doux et tonique\r\nau goût végétal et très légèrement iodé'),(6,'Velouté de potiron et orange',1,1,1,'Légumes de saison'),(7,'Croque monsieur au maïs violet',2,1,1,'sauce au maïs et pop corn épicés'),(8,'Cannelé chocolat épinard',3,1,1,'coulis d\'épinards sucrés'),(9,'Qimen Haoya',4,1,1,'Thé Noir de Chine Fruité et Boisé aux notes Florales\r\net délicatement Cacaotées'),(10,'Sencha Ariake',4,1,1,'Thé vert Japonais à la fois doux et tonique\r\nau goût végétal et très légèrement iodé');
/*!40000 ALTER TABLE `plat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `price` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (1,19),(2,24);
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `formation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Chloé','ALBY',18,'Parcours: \r\nBac pro service BTS MHR \r\nOption désirée option A service\r\nRôle dans l\'intrigue : actionnaire, barman et service des plats.\r\nMissions:\r\n- rédaction de la culture entreprise\r\n- création d\'un site internet\r\n- participation à la création du logo\r\n- participation aux choix des boissons','chloe.jpg','Vis pour ce que demain a à t\'offrir et non pour ce que hier t\'a enlevé'),(2,'Jessica','BELLIDO',21,'Parcours :\r\n- bac général\r\n- mise à niveau\r\n- BTS MHR\r\nOption désiré :\r\noption A service\r\nRôle dans l\'intrigue : actionnaire et cuisinière réalise les entrées et les plats avec BARBAOUAT.\r\nMissions :\r\n- conception et création de notre projet de \"kioskfood\" ainsi que le logo\r\n- contribution au dossier notamment dans l\'étude de la cible\r\n- contenu design et modélisation des menus\r\n- élaboration des plats et des entrée du menu','perso1.jpg','Si tu réfléchis trop, tu ne fait rien.'),(3,'Lily','ANGELE',20,'Parcours :\r\n- bac général\r\n- mise à niveau\r\n- BTS MHR\r\nOption désiré :\r\nOption C hébergement\r\nRôle dans l\'intrigue : actionnaire chargé de la communication et de la distribution des plats\r\n- réalisation du sondage\r\n- design et réalisation du logo\r\n- réalisation de la matrice SWOT de L\'intrigue\r\n- réalisation des fiches des boissons \r\n- design et réalisation des cartes','perso3.jpg','Profite à fond.'),(4,'Joane','ARRANS',18,'Parcours : \r\n- bac STHR\r\n- BTS MHR\r\nOption désirée : \r\noption A service\r\nRôle dans l\'intrigue : actionnaire et pâtissière élabore et réalise les desserts\r\nmissions :\r\n- conception et réalisation de notre projet de \"kiosk food court\" ainsi que du logo\r\n- contenu, design et modélisation des menus\r\n- élaboration des plats du menu\r\n- réalisation de l\'étude de la concurrence et de la zone de chalandise\r\n- étude documentaire sur la demande\r\n- participation à l\'élaboration des plats du menu','perso2.jpg','Vis au jour le jour.'),(5,'Damien','BARBAOUAT',21,'Parcours : \r\n- bac STHR\r\n- BTS MHR\r\nOption désirée : \r\n- option B cuisine\r\nRôle dans dans l\'intrigue : actionnaire et cuisinier élabore et réalise les entrée et les plats avec Bellido Jessica\r\nmissions : \r\n- conception et création de notre projet de \"kiosk food court\" ainsi que du logo\r\n- contribution au dossier notamment dans l\'élaboration des fiches techniques\r\n- réalisation de l\'analyse économique et du calcul des coûts de l\'intrigue\r\n- contenu design et modélisation des menus\r\n- élaboration des plats et des entrée du menus','IMG-20220119-WA0008.jpg','une phrase');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'Entrée'),(2,'Plat principal'),(3,'Délices Sucrés'),(4,'Boissons');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'olivieralby@gmail.com','[]','$2y$13$VV/2QG0e4w56LQPVD1O1nuWOBG0ESz6UDs0yBWjDvafHR/w2.v.5e','olivier','alby'),(2,'chloe@gmail.com','[]','$2y$13$O6nb2PdlFEKz1lDM/laIzuNBQnyJ4Q6dYCERiXYmordzFeviGuEjy','Chloe','Alby'),(3,'sandrine@gmail.com','[]','$2y$13$tJ/fgfiFYaHh.633YcvaPufTJNGeQhNFEongWYT.JVbEe2c28YwNO','Sandrine','Alby'),(4,'papa@gmail.com','[]','$2y$13$LjvmvqbOxLX50Ese5qS7eetCwbHvSqwhmFuZzx2WTaLnphHLNVP26','Jean','Alby'),(5,'coco@gmail.com','[]','$2y$13$RQsb2qgEZvMI.sKY8Xnz1.F117BhrWYiJIVlujPA2z0Z25j9haCLy','coco','gardies'),(6,'tonton@gmail.com','[]','$2y$13$DLN7J0c1yt8wQSP2shksvO0oiG1cpwENPvYhKmextWyv45D.dzYxW','tonton','gmail'),(7,'chloealby@icloud.com','[]','$2y$13$I3LkSSK6fP.y0JOWDrPJEOVRxpLkYSA.x/hvQdeefCC8oM.Ee7rxy','Chloé','Alby'),(8,'damien@gmail.com','[]','$2y$13$vhDYpddaJqywdtQWtegXR.Bqvx4hQMRp30fKircFsT0V6o9mW0hm.','Dalien','Mose'),(9,'nico@gmail.com','[]','$2y$13$wT.kDYBfJkvrgb5Qh0Jn5uRbxpezPQUVsO.JeMLGgsHUv491QBGlW','Nicolas','Escaich'),(10,'demo@gmail.com','[\"string\"]','$2y$13$WbuON.VQyAn6E.KlIDt8IumazsQ1G/DgT9XJ7lJ5eemwygOobQpW2','$2y$13$WbuON.VQyAn6E.KlIDt8IumazsQ1G/DgT9XJ7lJ5eemwygOobQpW2','Demo'),(11,'lily@gmail.com','[]','$2y$13$GqnOFeYZ5fkrbKQbGDFQw.Gg6IrQwj/pdNng6JaSPo.x7cHQPNKY.','Lily','Demo'),(12,'gg@jj','[]','$2y$13$Lwse0XRqdDJP497fG0MpWOLBiVz2NIDSLqdImAlVtCCR/NM.2PrV.','Jj','Jj'),(13,'jj@kkj','[]','$2y$13$qGhYld1/pbRVkf5dxszssOSkwS8AxkIERBtMA/Syt1nPzw77RpNqG','Jh','Hjkl'),(14,'kjk@jkj','[]','$2y$13$5Jr6rxfmS9EHamiqkZDMLein7rjkqFQd3UzEtTEBLjQh8pWBjcjgu','Uiu','Kll'),(15,'h@k','[]','$2y$13$X6WYnh612o35IHbcGDHqteTxd7FQ.rijn//VKY.6haxHuoWVJxDTe','V','H'),(16,'jkljlk@jkljklj','[]','$2y$13$DW656mKvmKIJpDnU5XUKPeSNX9xQwVo5Mowpe.tlO.FgrkHpjBcKq','Hjlhlkjhjlk','Hjhlhjh'),(17,'','[]','$2y$13$PjE4MPKmEW9k70h24gGEce2ott7UpPK4GY2i2d6E6cjWG3htVqZee','',''),(20,'dwwm20olivier@gmail.com','[]','$2y$13$rO.3.0gUJfBKqi1s.MTFNOHHzieLk1oMM9zpGaEQfEcZDLUinu1mq','Chloe','Alby');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-02 12:39:27
