-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: demo_soap
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20220117124957','2022-01-17 12:50:02',62),('DoctrineMigrations\\Version20220118220430','2022-01-18 22:04:54',105);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formation` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Chloé','ALBY',18,'Parcours: \r\nBac pro service BTS MHR \r\nOption désirée option A service\r\nRôle dans l\'intrigue : actionnaire, barman et service des plats.\r\nMissions:\r\n- rédaction de la culture entreprise\r\n- création d\'un site internet\r\n- participation à la création du logo\r\n- participation aux choix des boissons','chloe.jpg','\"Vis pour ce que demain a à t\'offrir et non pour ce qu\'hier t\'a enlevé\"'),(2,'Jessica','BELLIDO',21,'Parcours :\r\n- bac général\r\n- mise à niveau\r\n- BTS MHR\r\nOption désiré :\r\noption A service\r\nRôle dans l\'intrigue : actionnaire et cuisinière réalise les entrées et les plats avec BARBAOUAT.\r\nMissions :\r\n- conception et création de notre projet de \"kioskfood\" ainsi que le logo\r\n- contribution au dossier notamment dans l\'étude de la cible\r\n- contenu design et modélisation des menus\r\n- élaboration des plats et des entrée du menu','perso1.jpg','\"Si tu réfléchis trop, tu ne fait rien.\"'),(3,'Lily','ANGELE',20,'Parcours :\r\n- bac général\r\n- mise à niveau\r\n- BTS MHR\r\nOption désiré :\r\nOption C hébergement\r\nRôle dans l\'intrigue : actionnaire chargé de la communication et de la distribution des plats\r\n- réalisation du sondage\r\n- design et réalisation du logo\r\n- réalisation de la matrice SWOT de L\'intrigue\r\n- réalisation des fiches des boissons \r\n- design et réalisation des cartes','perso3.jpg','\"Profite à fond.\"'),(4,'Joane','ARRANS',18,'Parcours : \r\n- bac STHR\r\n- BTS MHR\r\nOption désirée : \r\noption A service\r\nRôle dans l\'intrigue : actionnaire et pâtissière élabore et réalise les desserts\r\nmissions :\r\n- conception et réalisation de notre projet de \"kiosk food court\" ainsi que du logo\r\n- contenu, design et modélisation des menus\r\n- élaboration des plats du menu\r\n- réalisation de l\'étude de la concurrence et de la zone de chalandise\r\n- étude documentaire sur la demande\r\n- participation à l\'élaboration des plats du menu','perso2.jpg','\"Vis au jour le jour.\"'),(5,'Damien','BARBAOUAT',21,'Parcours : \r\n- bac STHR\r\n- BTS MHR\r\nOption désirée : \r\n- option B cuisine\r\nRôle dans dans l\'intrigue : actionnaire et cuisinier élabore et réalise les entrée et les plats avec Bellido Jessica\r\nmissions : \r\n- conception et création de notre projet de \"kiosk food court\" ainsi que du logo\r\n- contribution au dossier notamment dans l\'élaboration des fiches techniques\r\n- réalisation de l\'analyse économique et du calcul des coûts de l\'intrigue\r\n- contenu design et modélisation des menus\r\n- élaboration des plats et des entrée du menus','IMG-20220119-WA0008.jpg','une phrase');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-23 20:43:17
