import {createStore} from "vuex"
import axios from "axios"

const store = createStore({
    state:{
        users:[],
        carts:[],
        menus:[]
    },
    getters:{
        getUsers(state){
            return state.users;
        },
        getCarts(state){
            return state.carts;
        },
        getMenus(state){
            return state.menus
        }
    },
    actions:{
        
        createUser({commit},data){
            axios.post('/api/users',{
                nom:data.user.nom,
                prenom:data.user.prenom,
                email:data.user.email,
                password:data.user.password,
            }).then(resp=>{
                commit('RESPUSER',resp.data);
            })
        },
        fetchMenu({commit},auth){
            axios.get('/api/menus.json',
            {
                headers:{
                    'Authorization':'Bearer '+ auth
                    }
            }
            )
            .then(  (response)=>{
                commit("RESPMENU",response.data);
                })
        },
        clearCart({commit}){
            commit('updateCart',[])
        }
       
    },

    mutations:{
        RESPUSER(state, users){
            state.users = users;
        },
        RESPORDER(state, orders){
            state.orders = orders
        },
        RESPMENU(state,menu){
            state.menus = menu
        },
        addToCart(state,product){
            state.carts.push(product)
        },
        removeToCart(state, index){
            state.carts.splice(index,1)
            console.log(state.carts)
        },
        updateCart(state,carts){
            state.carts = carts
        },
        
    }
})

export default store;