import {createApp} from 'vue'
import {createRouter, createWebHistory} from 'vue-router'
import Home from './Home.vue';
import store from './store'
import './bootstrap';
import { StripePlugin } from '@vue-stripe/vue-stripe';
import { library } from '@fortawesome/fontawesome-svg-core'

import { faHatWizard } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
let button =document.querySelector('.command');
if(button){
    button.addEventListener('click',()=>{
    window.scroll(0,0);
    
     const app = createApp(Home)

     const router = createRouter({
        history: createWebHistory(),
        routes: [
            
                    {
                      path:'/',
                      name:'home',
                      component:()=>import('./Home.vue'),
                      children:[
                    
                        {
                            path:'/create',
                            name:'create',
                            component:()=>import('./components/Create.vue'),
                        },
                        
                        {
                            path:'/menu',
                            name:'menu',
                            component:()=>import('./components/Command.vue'),
                            props:true,
                           
                        },
                        {
                            path:'/quantite/:id',
                            name:'qte',
                            component:()=>import('./components/Total.vue'),
                            props:true,
                        },
                        {
                            path:'/checkout',
                            name:'checkout',
                            component:()=>import('./components/Checkout.vue'),
                            props:true,
                        },
                        {
                            path:'/success',
                            name:'success',
                            component:()=>import('./components/Success.vue'),
                            props:true,
                        },
                    ],
                  }
                ],
            
        
    })
    

const options = {
  pk:'pk_test_51KVmSCB8Gk9uKNlVip71Qp637DFh5furg2WLvpslhRKLN2y10N7k5tYKjngyUiXzwLACS4jWTEVov1svAyebpOAf00TOxVzBrM',
  stripeAccount:'acct_1KVmSCB8Gk9uKNlV',
  apiVersion: '2020-08-27',
};

    //app.use(StripePlugin,options);
    app.use(router)
    app.use(store)
    app.mount('#app')
})
}

