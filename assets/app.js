/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';
//import Vue from 'vue';
/*
import Member from './components/Member.vue';
import Home from './Home.vue'
import Af from './components/Af.vue';
*/
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

//ajout de la div vue js

//new Vue({
//    components:{Af}
//}).$mount('#app')

document.querySelector('.hamburger').addEventListener('click',()=>{
    document.querySelector('._menu').classList.toggle('trans')
})

//document.querySelector('.arrow a').addEventListener('click',()=>{
 //   window.scroll(0,600);
//})

class Lightbox{

    static init(){
        let links = Array.from(document.querySelectorAll('.image a'))
        let gallery = links.map(element=>element.getAttribute('href'))

        links.forEach(element => {
            element.addEventListener('click',(e)=>{
                e.preventDefault();
                new Lightbox(e.currentTarget.getAttribute('href'),gallery)
            })
        });

    }

    constructor(url, gallery){
        this.element = this.buildForm(url)
        this.url = url
        this.gallery = gallery
        let div = document.querySelector('.container_show')
        div.appendChild(this.element)
        this.loadImage(url)

    }
    loadImage(url){
        const image = new Image()
        this.url = ''
        const container = document.querySelector('.lightbox_container')
        container.innerHTML = ""
        image.onload = ()=>{
            container.appendChild(image)
            this.url = url
        }
        image.classList.add('img-fluid')
        image.src = url
    }

    close(){
        this.element.parentElement.removeChild(this.element) 
    }

    prev(e){
        let pos = this.gallery.findIndex(image => image===this.url)
        if(pos===0){
            pos = this.gallery.length
        }
        this.loadImage(this.gallery[pos-1])
    }

    next(){
        let pos = this.gallery.findIndex(image => image === this.url)
        if(pos=== this.gallery.length - 1){
            pos = -1
        }
        this.loadImage(this.gallery[pos+1])
    }

    buildForm(url){
        const dom = document.createElement('div')
        dom.classList.add('gallery')
        dom.innerHTML = `
        <button class="close"><i class="fas fa-times fa-2x"></i></button>
        <button class="prev"><i class="fas fa-chevron-left fa-2x"></i></button>
        <div class="lightbox_container">
        <img src="${url}" />
        </div>
        <button class="next"><i class="fas fa-chevron-right fa-2x"></i></button>
        `
        dom.querySelector('.close').addEventListener('click',()=>{this.close()})
        dom.querySelector('.prev').addEventListener('click',this.prev.bind(this))
        dom.querySelector('.next').addEventListener('click',this.next.bind(this))
        return dom
    }

}

Lightbox.init()



//animation
const options = {
    root:null,
    rootMargin:"10px",
    threshold:.3
}


const handle = (entries,observer)=>{
    entries.forEach(entrie=>{
        if(entrie.intersectionRatio>.3){
            entrie.target.classList.add('reveal-visible')
            observer.unobserve(entrie.target)
        }
    })
}
const handle2 = (entries,observer)=>{
    entries.forEach(entrie=>{
        if(entrie.intersectionRatio>.3){
            entrie.target.classList.add('appear-visible')
            observer.unobserve(entrie.target)
        }
    })
}
const handle3 = (entries,observer)=>{

    entries.forEach(entrie=>{
        if(entrie.intersectionRatio==0){
            entrie.target.classList.add('menu-visible')
            observer.unobserve(entrie.target)
        }
        
    })
}
window.addEventListener('DOMContentLoaded',()=>{
    const observer = new IntersectionObserver(handle, options);
    const observer2 = new IntersectionObserver(handle2, options);
    const observer3 = new IntersectionObserver(handle3, {
        root:null,
        rootMargin:"0px",
        threshold:0.1
    });
    const targets = document.querySelectorAll('[class*="reveal-"]');
    const targets2 = document.querySelectorAll('[class*="appear-"]');
    const targets3 = document.querySelectorAll('[class*="menu-"]');
    targets.forEach(target => {
        observer.observe(target)
    });
    targets2.forEach(target => {
        observer2.observe(target)
    });
    targets3.forEach(target => {
        observer3.observe(target)
    });
    
})
