<h1>Lintrigue</h1>

Projet FoodPairring réalisé pour un stage de fin d'étude d'une classe du Lycée hôtellier de tourisme de Toulouse.

<h2>Site vitrine avec une partie e-commerce</h2>
        



Framework Symfony6 : <br>
    - EasyAdmin<br>
    - Mysql<br>
    - Php8<br>
    - Vue js pour la partie e-commerce<br>
    - Système d'authentification avec JWTLexicBundle<br>
    - instalation de l'api Stripe pour le paiement<br>

<h2>Cahier des charges</h2>
Site vitrine présente les concepts du food pairring.<br>
Présentation de l'équipe.<br>
<h3>e-commerce</h3>
Le client aura la possibilité de commander en ligne.<br>
Pour cela il devra d'abord créer un compte client.
Puis il commande les menus qui l'aura choisi.<br>
Et enfin il paye par carte bleu via l'api Stripe.<br>
Un email de confirmation lui sera envoyé.

Framework Symfony : <br>
    - EasyAdmin
    - Mysql
    - Php8
    - Vue js
    - Système d'authentification avec JWTLexicBundle et vue js