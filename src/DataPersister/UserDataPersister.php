<?php
namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\BlogPost;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $em;
    private UserPasswordHasherInterface $encoder;
    public function __construct(EntityManagerInterface $em, UserPasswordHasherInterface $encoder)
    {
        $this->em =$em;
        $this->encoder = $encoder;
    }
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function persist($data, array $context = []):void
    {
        // call your persistence layer to save $data

        $data->setPassword($this->encoder->hashPassword($data,$data->getPassword()));
        $data->setPrenom(ucfirst($data->getPrenom()));
        $data->setNom(ucfirst($data->getNom()));
        $this->em->persist($data);
        $this->em->flush();
       
    }

    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
    }
}