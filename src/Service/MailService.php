<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;


class MailService{

    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function envoieMail( $to,array $user,array $cart, int $total): void
    {
        $email = (new TemplatedEmail())
        ->from('olivieralby@gmail.com')
            ->to($to)
            
            ->subject('Commande Lintrigue')
            ->htmlTemplate('mail/index.html.twig')
            ->context([
                'user'=>$user,
                'cart'=>$cart,
                'total'=>$total
            ]);

        $this->mailer->send($email);    }
}