<?php

namespace App\Service;

use App\Entity\Menu;

class StripeService{

    private String $privatekey;

    public function __construct()
    {
    
        $this->privatekey="sk_test_51KVmSCB8Gk9uKNlVH5ge573Intx9fxiXxAdPczZBhjGMNDqPUmkOxvUsluIqvvcuye3LLoiQEEIdwZfFaGgvIF3H00zQVDEsr1";
    }

    public function paymentIntent(Int $price)
    {
        \Stripe\Stripe::setApiKey($this->privatekey);
        return \Stripe\PaymentIntent::create([
            'amount' => $price*100,
            'currency' => 'eur',
            'payment_method_types' => ['card']
        ]);
    }

    public function paiement(
        $amount,
        $currency,
        $description,
        array $stripeParameter
    ): \Stripe\PaymentIntent
    {
        \Stripe::setApiKey($this->privateKey);

        $payment_intent = null;

        if(isset($stripeParameter['stripeIntentId'])) {
            $payment_intent = \Stripe\PaymentIntent::retrieve($stripeParameter['stripeIntentId']);
        }

        if($stripeParameter['stripeIntentStatus'] === 'succeeded') {
            //TODO
        } else {
            $payment_intent->cancel();
        }

        return $payment_intent;
    }

    /**
     * @param array $stripeParameter
     * @param Menu $menu
     * @return \Stripe\PaymentIntent|null
     */
    public function stripe(array $stripeParameter, Menu $menu): ?\Stripe\PaymentIntent
    {
        return $this->paiement(
            $menu->getPrice() * 100,
            'eur',
            $menu->getName(),
            $stripeParameter
        );
    }
}