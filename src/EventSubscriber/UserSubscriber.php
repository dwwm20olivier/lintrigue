<?php

namespace App\EventSubscriber;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserSubscriber implements EventSubscriberInterface
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function persitEventEasy(BeforeEntityPersistedEvent $event)
    {
        // ...
        $entity = $event->getEntityInstance();
        if (!$entity instanceof User) {
            return;
        }
        $entity->setPrenom(ucfirst($entity->getPrenom()));
        $entity->setNom(strtoupper($entity->getNom()));
        $entity->setPassword($this->encoder->hashPassword($entity, $entity->getPassword()));
    }
// return the subscribed events, their methods and priorities
    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['persitEventEasy'],
        ];
    }
}
