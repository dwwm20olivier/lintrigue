<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MenuRepository::class)]
 #[ApiResource(
                       collectionOperations:['get'=>
                           ['normalization_context'=>['groups'=>['menu:read','price:read']]]
                   ],
                       itemOperations:[
                           'get'=>['normalization_context'=>['groups'=>['menu:read','price:read']]]
                       ]
                  )]
class Menu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('menu:read')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('menu:read')]
    private $name;

    #[ORM\ManyToOne(targetEntity: Price::class, inversedBy: 'menus', fetch:"EAGER")]
    #[Groups('menu:read')]
    private $price;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('menu:read')]
    private $label;

    #[ORM\OneToMany(mappedBy: 'menu', targetEntity: Plat::class, fetch:"EAGER")]
    private $plats;

    #[ORM\OneToMany(mappedBy: 'menu', targetEntity: Order::class)]
    #[Groups('menu:read')]
    private $quant;

    

    public function __construct()
    {
        $this->plats = new ArrayCollection();
        $this->quant = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?Price
    {
        return $this->price;
    }

    public function setPrice(?Price $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Plat[]
     */
    public function getPlats(): Collection
    {
        return $this->plats;
    }

    public function addPlat(Plat $plat): self
    {
        if (!$this->plats->contains($plat)) {
            $this->plats[] = $plat;
            $plat->setMenu($this);
        }

        return $this;
    }

    public function removePlat(Plat $plat): self
    {
        if ($this->plats->removeElement($plat)) {
            // set the owning side to null (unless already changed)
            if ($plat->getMenu() === $this) {
                $plat->setMenu(null);
            }
        }

        return $this;
    }
    public function __toString(){
    
        return $this->name;
    }

    public function getQuant(): Collection
    {
        return $this->quant;
    }

    public function addQuant(Order $quant): self
    {
        if (!$this->quant->contains($quant)) {
            $this->quant[] = $quant;
            $quant->setMenu($this);
        }

        return $this;
    }

    public function removeQuant(Order $quant): self
    {
        if ($this->quant->removeElement($quant)) {
            // set the owning side to null (unless already changed)
            if ($quant->getMenu() === $this) {
                $quant->setMenu(null);
            }
        }

        return $this;
    }

    
}
