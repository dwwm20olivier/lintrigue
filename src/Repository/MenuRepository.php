<?php

namespace App\Repository;

use App\Entity\Menu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Menu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Menu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Menu[]    findAll()
 * @method Menu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    // /**
    //  * @return Menu[] Returns an array of Menu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function menu(int $value): ?array
    {
        return $this->createQueryBuilder('m')

            ->join('m.plats','p')
            ->join('m.price','pri')
            ->join('p.type','t')
            ->select('m.name, p.label, p.comment, pri.label as price, t.label as type')
            ->andWhere('m.id = :val')
            ->setParameter('val',$value)
            ->getQuery()
            ->getResult()
        ;
    }
    public function price(int $value)
    {
        return $this->createQueryBuilder('m')
            ->join('m.price','pri')
            ->select(' pri.label as price')
            ->andWhere('m.id = :val')
            ->setParameter('val',$value)
            ->groupBy('price')
            ->getQuery()
            ->getSingleResult()
        ;
    }  
    
    public function getMenuPrice(int $value){
        return $this->createQueryBuilder('m')
            ->join('m.price','pri')
            ->select(' m.name, pri.label as price')
            ->andWhere('m.id = :val')
            ->setParameter('val',$value)
            ->groupBy('price')
            ->getQuery()
            ->getResult()
        ;
    }
   
    
}
