<?php

namespace App\Manager;

use App\Entity\Menu;
use App\Entity\User;
use App\Service\StripeService;
use Doctrine\ORM\EntityManagerInterface;


class MenuManager{

    private $em;
    private $stripeService;

    public function __construct(EntityManagerInterface $em, StripeService $stripeService)
    {
        $this->em = $em;
        $this->stripeService = $stripeService;
    }

    public function getMenus()
    {
        return $this->em->getRepository(Menu::class)
            ->findAll();
    }

    public function intentSecret(Int $price)
    {
        $intent = $this->stripeService->paymentIntent($price);

        return $intent['client_secret'] ?? null;
    }
/*
    public function stripe(array $stripeParameter, Menu $menu)
    {
        $resource = null;
        $data = $this->stripeService->stripe($stripeParameter, $menu);

        if($data) {
            $resource = [
                'stripeBrand' => $data['charges']['data'][0]['payment_method_details']['card']['brand'],
                'stripeLast4' => $data['charges']['data'][0]['payment_method_details']['card']['last4'],
                'stripeId' => $data['charges']['data'][0]['id'],
                'stripeStatus' => $data['charges']['data'][0]['status'],
                'stripeToken' => $data['client_secret']
            ];
        }

        return $resource;
    }

    public function create_subscription(array $resource, Menu $menu, User $user)
    {
        // $order = new Order();
        // $order->setUser($user);
        // $order->setProduct($product);
        // $order->setPrice($product->getPrice());
        // $order->setReference(uniqid('', false));
        // $order->setBrandStripe($resource['stripeBrand']);
        // $order->setLast4Stripe($resource['stripeLast4']);
        // $order->setIdChargeStripe($resource['stripeId']);
        // $order->setStripeToken($resource['stripeToken']);
        // $order->setStatusStripe($resource['stripeStatus']);
        // $order->setUpdatedAt(new \Datetime());
        // $order->setCreatedAt(new \Datetime());
        // $this->em->persist($order);
        // $this->em->flush();
    }
    */
}