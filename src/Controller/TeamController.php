<?php

namespace App\Controller;

use App\Repository\TeamsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    #[Route('/team', name: 'team')]
    public function index(TeamsRepository $teamsRepository): Response
    {
        
        return $this->render('team/index.html.twig', [
            'teams' => $teamsRepository->findAll(),
        ]);
    }
}
