<?php

namespace App\Controller;

use App\Service\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\HttpFoundation\Request;



class MailController extends AbstractController
{
    
    #[Route('/mail', name: 'mail')]
    public function index( MailerInterface $mailer, Request $request, MailService $service): Response
    {
        $data = $request->getContent();
        $data = \json_decode($data, true);
        $menu =$data['menu'];
        $total = $data['total'];
        $user = $data['user'];
        $to = $user['email'];
        $service->envoieMail($to, $user, $menu, $total);
       
        return $this->JsonResponse('envoie réussi');
    }
}
