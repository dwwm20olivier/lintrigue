<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MenuRepository;

class MenuController extends AbstractController
{
    #[Route('/menu', name: 'menu')]
    public function index(MenuRepository $repository): Response
    {
        $decouvertes = $repository->menu(2);
        $pricedecouv = $repository->price(2);
        $aventures = $repository->menu(1);
        $priceA = $repository->price(1);
        return $this->render('menu/index.html.twig', [
            'decouvertes' => $repository->findAll(),
            'priceD' => $pricedecouv,
            'aventures'=> $aventures,
            'priceA'=>$priceA
        ]);
    }
}
