<?php

namespace App\Controller\Admin;

use App\Entity\Teams;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;


class TeamsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Teams::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            
            TextField::new('prenom'),
            TextField::new('nom'),
            IntegerField::new('age'),
            TextareaField::new('description'),
            TextareaField::new('formation'),
            ImageField::new('image')->setBasePath('/upload/images')->setUploadDir('public/upload/images'),
            Field::new('imageFile')->setFormType(VichImageType::class)->hideOnForm(),

        ];
    }
    
}
