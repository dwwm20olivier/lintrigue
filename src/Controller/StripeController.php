<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Menu;
use App\Repository\MenuRepository;
use App\Manager\MenuManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
class StripeController extends AbstractController

{

   
    
    #[Route('/stripe', name: 'stripe')]
    public function paiment( MenuManager $manager, Request $request, SerializerInterface $serializer ): Response
    {
         $menu = $request->getContent();
         $data = \json_decode($menu,true);

         $price = ($data['price']);
        
        return new JsonResponse([
            'clientSecret'=>$manager->intentSecret($price)
        ]);
        // return new JsonResponse([
        //     'app'=> $this->renderView('stripe/index.html.twig', [
        //         'user'=>$user,
        //         "menu"=>$menu,
        //         "intentSecret" => $manager->intentSecret($menu),
        //     ])
        // ]);
    

        // return $this->render('stripe/index.html.twig', [
        //     'user'=>$user['user'],
        //     "menu"=>$menu,
        //     "intentSecret" => $manager->intentSecret($menu),
        // ]);
    
    }

    // #[Route("/subscription/{id}", name:"subscription")]
    // public function subscription(Menu $menu, Request $request,MenuManager $manager ): Response
    // {
    //     $user = $this->getUser();

    //     if($request->getMethod() === "POST") {
    //         $resource = $manager->stripe($_POST, $menu);

    //         if(null !== $resource) {
    //             $manager->create_subscription($resource, $menu, $user);

    //             return $this->render('stripe/reponse.html.twig', [
    //                 'product' => $menu
    //             ]);
    //         }
    //     }

    //     return $this->redirectToRoute('payment', ['id' => $menu->getId()]);
    // }
}
